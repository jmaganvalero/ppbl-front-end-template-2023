import { gql } from "@apollo/client";

export const METADATA_KEY_QUERY = gql`
  query TransactionsWithMetadataKey($metadatakey: String!) {
    transactions(where: { metadata: { key: { _eq: $metadatakey } } }, order_by: { includedAt: desc }) {
      hash
      includedAt
      metadata {
        key
        value
      }
    }
  }
`;

export const ADDRESS_METADATA_QUERY = gql`
  query txFromWalletAddressWithMetadataKey($walletAddress: String!, $metadataKey: String!) {
    transactions(
      where: { _and: [{ inputs: { address: { _eq: $walletAddress } } }, { metadata: { key: { _eq: $metadataKey } } }] }
    ) {
      hash
      includedAt
      metadata {
        key
        value
      }
    }
  }
`;

export const PPBL_NFT_METADATA_QUERY = gql`
  query GetPPBL2023NFTMetadata($tokenPolicyId: Hash28Hex!) {
    transactions(
      where: {
        _and: [{ mint: { asset: { policyId: { _eq: $tokenPolicyId } } } }, { metadata: { key: { _eq: "721" } } }]
      }
      order_by: { includedAt: desc }
    ) {
      hash
      includedAt
      mint {
        asset {
          tokenMints {
            asset {
              assetName
              assetId
            }
            quantity
          }
        }
      }
      metadata {
        key
        value
      }
    }
  }
`;
